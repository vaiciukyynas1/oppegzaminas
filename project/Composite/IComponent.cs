using System.Net.Http.Headers;
using System.Security;

namespace project.Composite;

public abstract class IComponent
{
    public IComponent()
    {

    }

    public abstract string Operation();

    public virtual void Add(IComponent component)
    {
        throw new NotImplementedException();
    }

    public virtual void Remove(IComponent component)
    {
        throw new NotImplementedException();
    }

    public virtual bool IsComposite()
    {
        return true;
    }
}

public class Leaf : IComponent
{
    public override string Operation()
    {
        return "Leaf";
    }

    public override bool IsComposite()
    {
        return false;
    }
}

class Composite : IComponent
    {
        protected List<IComponent> _children = new List<IComponent>();

        public override void Add(IComponent component)
        {
            this._children.Add(component);
        }

        public override void Remove(IComponent component)
        {
            this._children.Remove(component);
        }

        // The Composite executes its primary logic in a particular way. It
        // traverses recursively through all its children, collecting and
        // summing their results. Since the composite's children pass these
        // calls to their children and so forth, the whole object tree is
        // traversed as a result.
        public override string Operation()
        {
            int i = 0;
            string result = "Branch(";

            foreach (IComponent component in this._children)
            {
                result += component.Operation();
                if (i != this._children.Count - 1)
                {
                    result += "+";
                }
                i++;
            }

            return result + ")";
        }
    }
