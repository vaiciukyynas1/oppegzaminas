namespace project.Composite;

public class Client
{
    public void ClientCode(IComponent leaf)
    {
        Console.WriteLine($"Result: {leaf.Operation()}");
    }

    public void ClientCode2(IComponent component1, IComponent component2)
    {
        if (component1.IsComposite())
        {
            component1.Add(component2);
        }

        Console.WriteLine($"Result: {component1.Operation()}");
    }
}
