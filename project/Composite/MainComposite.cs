using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project.Composite;

public class MainComposite
{
    public static void Execute()
    {
        var client = new Client();

        var leaf = new Leaf();
        Console.WriteLine("Client: I get a simple component");
        client.ClientCode(leaf);

        var tree = new Composite();
        var branch1 = new Composite();
        branch1.Add(new Leaf());
        branch1.Add(new Leaf());
        var branch2 = new Composite();
        branch2.Add(new Leaf());
        tree.Add(branch1);
        tree.Add(branch2);
        Console.WriteLine("Client: I get a composite tree");
        client.ClientCode(tree);

        client.ClientCode2(tree, leaf);
    }
}
