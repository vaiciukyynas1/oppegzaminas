namespace project.Examples.Second;

public class UnitFlyweight
{
    private string unitImage;

    public UnitFlyweight(string unitImage)
    {
        this.unitImage = unitImage;
    }

    public void Operation(IUnit unit)
    {
        var imgChars = this.unitImage.Take(15);
        Console.WriteLine($"Unit coords: {unit.GetX()}x{unit.GetY()}, image: {string.Join("", imgChars)}");
    }
}
