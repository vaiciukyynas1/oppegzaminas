namespace project.Examples.Second;

public class Plane : IUnit
{
    private int[] unitCoordinates;

    public Plane(int[] coords)
    {
        this.unitCoordinates = coords;
    }

    public int GetX()
    {
        return this.unitCoordinates[0];
    }

    public int GetY()
    {
        return this.unitCoordinates[1];
    }
}
