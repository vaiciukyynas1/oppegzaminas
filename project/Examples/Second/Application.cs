using Microsoft.VisualBasic;

namespace project.Examples.Second;

public class Application
{
    /// <summary>
    /// Executes main logic of the design pattern. Creates needed objects, calls needed methods.
    /// </summary>
    public static void Execute()
    {
        var factory = new UnitFactory();
        var plane = factory.GetFlyweight(0);
        var ship = factory.GetFlyweight(1);
        var tank = factory.GetFlyweight(2);
        plane.Operation(new Plane(new int[] {0, 0}));
        plane.Operation(new Plane(new int[] {0, 2}));
        plane.Operation(new Plane(new int[] {5, 4}));
        ship.Operation(new Ship(new int[] {0, 0}));
        ship.Operation(new Ship(new int[] {0, 2}));
        ship.Operation(new Ship(new int[] {5, 4}));
        tank.Operation(new Tank(new int[] {0, 0}));
        tank.Operation(new Tank(new int[] {0, 2}));
        tank.Operation(new Tank(new int[] {5, 4}));
    }
}
