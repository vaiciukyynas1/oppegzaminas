namespace project.Examples.Second;

public class Tank : IUnit
{
    private int[] unitCoordinates;

    public Tank(int[] coords)
    {
        this.unitCoordinates = coords;
    }

    public int GetX()
    {
        return this.unitCoordinates[0];
    }

    public int GetY()
    {
        return this.unitCoordinates[1];
    }
}
