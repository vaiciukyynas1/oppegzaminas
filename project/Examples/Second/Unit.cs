namespace project.Examples.Second;

public interface IUnit
{
    int GetX();

    int GetY();
}
