namespace project.Examples.Second;

public class UnitFactory
{
    private Dictionary<int, UnitFlyweight> _flyweights = new();

    public UnitFactory()
    {
        this._flyweights.Add(0, new(new string('A', 5000)));
        this._flyweights.Add(1, new(new string('B', 5000)));
        this._flyweights.Add(2, new(new string('C', 5000)));
    }

    public UnitFlyweight GetFlyweight(int unitId)
    {
        return this._flyweights[unitId];
    }
}
