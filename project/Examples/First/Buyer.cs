using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project.Examples.First;

public class Buyer : Colleague
{
    public Buyer(Mediator mediator) : base(mediator)
    {
    }

    public void buy()
    {
        Console.WriteLine($"Buying some");
    }
}
