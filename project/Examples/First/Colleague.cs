namespace project.Examples.First;

public class Colleague
{
    private Mediator _mediator;

    public Colleague(Mediator mediator)
    {
        this._mediator = mediator;
    }

    public void action(string action)
    {
        this._mediator.action(action);
    }
}
