using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project.Examples.First;

public class Seller : Colleague
{
    public Seller(Mediator mediator) : base(mediator)
    {
    }

    public void sell()
    {
        Console.WriteLine($"Selling something");
    }
}
