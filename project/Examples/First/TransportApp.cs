using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace project.Examples.First;

public class TransportApp
{
    public static void Execute()
    {
        var broker = new TransportBroker();
        var bank = new Bank(broker);
        var buyer = new Buyer(broker);
        var seller = new Seller(broker);
        var customs = new Customs(broker);

        broker.Bank = bank;
        broker.Buyer = buyer;
        broker.Seller = seller;
        broker.Customs = customs;

        bank.action("sell");
    }
}
