namespace project.Examples.First;

public class Bank : Colleague
{
    public Bank(Mediator mediator) : base(mediator)
    {
    }

    public void makeTransaction()
    {
        Console.WriteLine($"Making transaction for something");
    }
}
