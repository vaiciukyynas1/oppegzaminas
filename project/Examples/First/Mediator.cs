using project.ChainOfResponsibility;

namespace project.Examples.First;

public abstract class Mediator
{
    /// <summary>
    /// Calls actions to execute.
    /// </summary>
    /// <param name="action">What action to execute.</param>
    public abstract void action(string action);
}
