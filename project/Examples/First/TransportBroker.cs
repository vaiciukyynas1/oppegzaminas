using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace project.Examples.First;

public class TransportBroker : Mediator
{
    public Bank Bank { get; set; } = null!;

    public Buyer Buyer { get; set; } = null!;

    public Customs Customs { get; set; } = null!;

    public Seller Seller { get; set;} = null!;

    public override void action(string action)
    {
        if (action == "buy")
        {
            this.Buyer .buy();
        }
        else if(action == "sell")
        {
            this.Seller.sell();
        }
        else if(action == "confirm")
        {
            this.Customs .confirm();
        }
        else if(action == "makeTransaction")
        {
            this.Bank .makeTransaction();
        }
    }
}
