using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace project.ChainOfResponsibility;

public class Client
{
    public void ClientCode(AbstractHandler handler)
    {
        foreach (var food in new List<string>() { "Nut", "Banana", "Cup of coffe" })
        {
            Console.WriteLine($"Client: who wants a {food}");

            var result = handler.Handle(food);

            if (result is not null)
            {
                Console.WriteLine($"{result}");
            }
            else
            {
                Console.WriteLine($"{food} was untouched");
            }
        }
    }
}
