using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualBasic;

namespace project.ChainOfResponsibility;

public class MainCor
{
    public static void Execute()
    {
        var client = new Client();

        var monkey = new MonkeyHandler();
        var squirrel = new SquirrelHandler();
        var dog = new DogHandler();

        monkey.SetNext(squirrel).SetNext(dog);

        Console.WriteLine("Chain: Monkey > Squirrel > Dog\n");
        client.ClientCode(monkey);
        Console.WriteLine();

        Console.WriteLine("Subchain: Squirrel > Dog\n");
        client.ClientCode(squirrel);
    }
}
