using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project.ChainOfResponsibility;

public class MonkeyHandler : AbstractHandler
{
    public override object? Handle(object request)
    {
        if ((request as string) == "Banana")
        {
            return $"Monkey: I'll eat the {request.ToString()}.\n";
        }

        return base.Handle(request);
    }
}

public class SquirrelHandler : AbstractHandler
{
    public override object? Handle(object request)
    {
        if (request.ToString() == "Nut")
        {
            return $"Squirrel: I'll eat the {request.ToString()}.\n";
        }
        else
        {
            return base.Handle(request);
        }
    }
}

public class DogHandler : AbstractHandler
{
    public override object? Handle(object request)
    {
        if (request.ToString() == "MeatBall")
        {
            return $"Dog: I'll eat the {request.ToString()}.\n";
        }
        else
        {
            return base.Handle(request);
        }
    }
}