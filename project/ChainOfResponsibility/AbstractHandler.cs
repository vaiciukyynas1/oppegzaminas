using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace project.ChainOfResponsibility;

public class AbstractHandler : IHandler
{
    private IHandler? _nextHandler;

    public IHandler SetNext(IHandler handler)
    {
        this._nextHandler = handler;

        return handler;
    }

    public virtual object? Handle(object request)
    {
        if (this._nextHandler is not null)
        {
            return this._nextHandler.Handle(request);
        }

        return null;
    }
}
