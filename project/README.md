# Template method

Defines the skeleton of an algorithm in the superclass but lets subclasses override specific steps of the algorithm without changing it's structure.

In short:
1. You have some kind of algorithm, but which has many available steps
2. You can make some steps overridable and then the new class can override those new steps

# Examples

Done: flyweight, mediator
Need to do: Chain of responsibility

# TODO

Add UML diagrams near examples